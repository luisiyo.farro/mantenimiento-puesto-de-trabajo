jQuery.sap.require("sap.m.MessageBox");
sap.ui.define([
	"sap/ui/sta/controller/BaseController",
	"sap/m/MessageToast",
	"sap/ui/core/Fragment"
], function (BaseController, MessageToast, Fragment) {
	"use strict";
	var Z_OD_SCP_BASRRC001_TAB_SRV;
	var Z_OD_SCP_CORE_0001_SRV;
	var thes;
	return BaseController.extend("sap.ui.sta.controller.Home", {

		onInit: function (oEvent) {

			thes = this;
			Z_OD_SCP_BASRRC001_TAB_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BASRRC001_TAB_SRV");
			this.getView().setModel(Z_OD_SCP_BASRRC001_TAB_SRV);
			Z_OD_SCP_CORE_0001_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_CORE_0001_SRV");
			this.getView().setModel(Z_OD_SCP_CORE_0001_SRV, "CORE_0001");
		},

		onItemPress: function (oEvent) {
			var reg = oEvent.getParameters().item.getBindingContext().getProperty();
			this.getRouter().navTo("detalle", {
				equip: reg.equip
			});
		},

		onCrearEquipo: function (oEvent) {
			var newEquipo = thes.getView().getModel("modeloEquipo").getData();
			
			if (newEquipo.equip === "") {
				sap.m.MessageBox.error("Ingrese el equipo");
				return;
			}
			
			if (newEquipo.bukrs === "") {
				sap.m.MessageBox.error("Ingrese la sociedad");
				return;
			}

			if (newEquipo.werks === "") {
				sap.m.MessageBox.error("Ingrese el centro");
				return;
			}

			newEquipo.begda = thes.dateToString(newEquipo.begda);
			newEquipo.endda = thes.dateToString(newEquipo.endda);

			Z_OD_SCP_BASRRC001_TAB_SRV.create("/EquiposTablerosSet", newEquipo, {
				success: function (result) {
					sap.m.MessageBox.success("Fue creado con éxito!");
					thes.getView().getModel().refresh();
					thes.byId("LineItemsSmartTable").getTable().clearSelection();
					thes.byId("dlgAgregarEquipo").close();
				},
				error: function (err) {
					sap.m.MessageBox.error("Error al crear");
					thes.getView().getModel().refresh();
					thes.byId("LineItemsSmartTable").getTable().clearSelection();
					thes.byId("dlgAgregarEquipo").close();
				}
			});

		},

		onEliminarEquipo: function (oEvent) {
			var index = thes.byId("LineItemsSmartTable").getTable().getSelectedIndex();

			if (index < 0) {
				return;
			}

			var path = thes.byId("LineItemsSmartTable").getTable().getContextByIndex(index).getPath();

			path = path.replace("ZCDS_VW_BASRRC001_061", "EquiposTablerosSet");

			Z_OD_SCP_BASRRC001_TAB_SRV.remove(path, {
				success: function (result) {
					sap.m.MessageBox.success("Fue eliminado con éxito!");
					thes.byId("LineItemsSmartTable").getTable().clearSelection();
					thes.getView().getModel().refresh();
				},
				error: function (err) {
					sap.m.MessageBox.error("Error al eliminar");
					thes.byId("LineItemsSmartTable").getTable().clearSelection();
					thes.getView().getModel().refresh();
				}
			});

		},

		newEquipo: function () {
			return {
				equip: "",
				bukrs: "",
				werks: "",
				descripcion: "",
				begda: null,
				endda: null
			};
		},

		onChangeSociedad: function (oEvent) {
			var bukrs = oEvent.getSource().getSelectedKey();
			thes.getCentro(bukrs);
		},

		getCentro: function (bukrs) {

			var filters = [];
			var filter = new sap.ui.model.Filter("Bukrs", sap.ui.model.FilterOperator.EQ, bukrs);
			filters.push(filter);

			Z_OD_SCP_CORE_0001_SRV.read("/CentrosSet", {
				filters: filters,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "modelCentro");
				},
				error: function (err) {

				}
			});

		},

		dlgCrearEquipo: function (oEvent) {
			var id = oEvent.getSource().data("action");

			switch (id) {
			case "C":
				var jsonModel = new sap.ui.model.json.JSONModel(thes.newEquipo());
				thes.getView().setModel(jsonModel, "modeloEquipo");
				break;
			case "E":
				var index = thes.byId("LineItemsSmartTable").getTable().getSelectedIndex();

				if (index < 0) {
					return;
				}

				var path = thes.byId("LineItemsSmartTable").getTable().getContextByIndex(index).getPath();
				var reg = thes.byId("LineItemsSmartTable").getTable().getContextByIndex(index).getProperty(path);

				//reg.begda = thes.dateToString(reg.begda);
				//reg.endda = thes.dateToString(reg.endda);

				if (reg.begda) {
					reg.begda.setDate(reg.begda.getDate() + 1);
				}
				if (reg.endda) {
					reg.endda.setDate(reg.endda.getDate() + 1);
				}

				thes.getCentro(reg.bukrs);

				delete reg["__metadata"];
				var jsonModel = new sap.ui.model.json.JSONModel(reg);
				thes.getView().setModel(jsonModel, "modeloEquipo");
				break;
			}

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgAgregarEquipo")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "sap.ui.sta.view.AgregarEquipo",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgAgregarEquipo").open();
			}

			switch (id) {
			case "C":
				thes.byId("dlgAgregarEquipo").setIcon("sap-icon://add");
				thes.byId("dlgAgregarEquipo").setTitle("Crear Equipo");
				thes.byId("iptEquipo").setEnabled(true);
				break;
			case "E":
				thes.byId("dlgAgregarEquipo").setIcon("sap-icon://edit");
				thes.byId("dlgAgregarEquipo").setTitle("Editar Equipo");
				thes.byId("iptEquipo").setEnabled(false);
				break;
			}

		}

	});
});