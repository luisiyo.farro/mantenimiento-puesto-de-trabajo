jQuery.sap.require("sap.m.MessageBox");
sap.ui.define([
	"sap/ui/sta/controller/BaseController",
	"sap/m/MessageToast",
	"sap/ui/core/Fragment"
], function (BaseController, MessageToast, Fragment) {
	"use strict";
	var Z_OD_SCP_BASRRC001_TAB_SRV;
	var Z_OD_SCP_BASRRC001_ORD_SRV;
	var thes;
	var werks;
	return BaseController.extend("sap.ui.sta.controller.Detalle", {

		onInit: function () {

			var oRouter = this.getRouter();
			oRouter.getRoute("detalle").attachMatched(this._onRouteMatched, this);

		},

		onBeforeRebindTablaEquipos: function (oEvent) {
			var binding = oEvent.getParameter("bindingParams");
			var oFilter = new sap.ui.model.Filter("equip", sap.ui.model.FilterOperator.EQ, thes.equip);
			binding.filters.push(oFilter);
			thes.getView().getModel().refresh();
		},

		_onRouteMatched: function (oEvent) {
			thes = this;
			Z_OD_SCP_BASRRC001_TAB_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BASRRC001_TAB_SRV");
			this.getView().setModel(Z_OD_SCP_BASRRC001_TAB_SRV);

			Z_OD_SCP_BASRRC001_ORD_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BASRRC001_ORD_SRV");

			var oArgs = oEvent.getParameter("arguments");
			thes.equip = oArgs.equip;

			var filters = [];
			var filter = new sap.ui.model.Filter("equip", sap.ui.model.FilterOperator.EQ, oArgs.equip);
			filters.push(filter);

			Z_OD_SCP_BASRRC001_TAB_SRV.read("/ZCDS_VW_BASRRC001_061", {
				filters: filters,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results[0]);

					thes.werks = result.results[0].werks;
					thes.equip = result.results[0].equip;

					if (result.results[0].begda) {
						result.results[0].begda.setDate(result.results[0].begda.getDate() + 1);
					}
					if (result.results[0].endda) {
						result.results[0].endda.setDate(result.results[0].endda.getDate() + 1);
					}

					thes.byId("ObjectHeaderDetalle").setModel(jsonModel);
				},
				error: function (err) {

				}
			});

			this.byId("smartTablePuestoTrabajo").rebindTable();

		},

		dlgCrearPuestoTrabajo: function (oEvent) {
			var id = oEvent.getSource().data("action");

			switch (id) {
			case "C":
				var jsonModel = new sap.ui.model.json.JSONModel(thes.newPuestoTrabajo());
				thes.getView().setModel(jsonModel, "modeloPuestoTrabajo");
				break;
			case "E":
				var index = thes.byId("smartTablePuestoTrabajo").getTable().getSelectedIndex();

				if (index < 0) {
					return;
				}

				var path = thes.byId("smartTablePuestoTrabajo").getTable().getContextByIndex(index).getPath();
				var reg = thes.byId("smartTablePuestoTrabajo").getTable().getContextByIndex(index).getProperty(path);

				var jsonModel = new sap.ui.model.json.JSONModel(reg);
				thes.getView().setModel(jsonModel, "modeloPuestoTrabajo");
				break;
			}

			var oView = this.getView();

			// create dialog lazily
			if (!this.byId("dlgAgregarPuestoTrabajo")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "sap.ui.sta.view.AgregarPuestoTrabajo",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
			} else {
				this.byId("dlgAgregarPuestoTrabajo").open();
			}

			switch (id) {
			case "C":
				thes.byId("dlgAgregarPuestoTrabajo").setIcon("sap-icon://add");
				thes.byId("dlgAgregarPuestoTrabajo").setTitle("Crear Puesto de Trabajo");
				thes.byId("cbPuestoTrabajo").setVisible(true);
				break;
			case "E":
				thes.byId("dlgAgregarPuestoTrabajo").setIcon("sap-icon://edit");
				thes.byId("dlgAgregarPuestoTrabajo").setTitle("Editar Puesto de Trabajo");
				thes.byId("cbPuestoTrabajo").setSelectedKey(reg.objid);
				thes.byId("cbPuestoTrabajo").setVisible(false);
				break;
			}

			thes.obtenerPuestosTrabajos();

		},

		onGuardarEquipo: function (oEvent) {
			//var proceso = oEvent.getSource().data("proceso");

			var objid = this.byId("cbPuestoTrabajo").getSelectedKey();
			var objty = "A";
			var principal = this.byId("ckbPrincipal").getSelected();
			var json = {
				equip: thes.equip,
				objty: objty,
				objid: objid,
				principal: principal
			};

			Z_OD_SCP_BASRRC001_TAB_SRV.create("/PuestoTrabajoSet", json, {
				success: function (result, status) {
					thes.byId("dlgAgregarPuestoTrabajo").close();
					thes.byId("smartTablePuestoTrabajo").getModel().refresh();

					var mensaje = JSON.parse(status.headers["sap-message"]);
					var mensajes = [];

					if (mensaje.length) {
						for (var i = 0; i < mensaje.length; i++) {
							mensajes.push({
								"Type": mensaje[i].severity,
								"Message": mensaje[i].message
							});
						}
					} else {
						mensajes.push({
							"Type": mensaje.severity,
							"Message": mensaje.message
						});
					}

					thes.mostrarMensajes(mensajes);

				},
				error: function (error) {
					thes.byId("dlgAgregarPuestoTrabajo").close();
					thes.bydId("smartTablePuestoTrabajo").getModel().refresh();
					sap.m.MessageBox.error("Error al guardar");
				}
			});

		},

		obtenerPuestosTrabajos: function () {

			var filters = [];
			var filter = new sap.ui.model.Filter("equip", sap.ui.model.FilterOperator.EQ, thes.equip);
			filters.push(filter);

			Z_OD_SCP_BASRRC001_TAB_SRV.read("/ZCDS_VW_BASRRC001_062", {
				filters: filters,
				success: function (result) {
					var filters = [];
					var andFilter = [];

					var filter = new sap.ui.model.Filter("werks", sap.ui.model.FilterOperator.EQ, thes.werks);
					filters.push(filter);

					for (var i = 0; i < result.results.length; i++) {
						var filter = new sap.ui.model.Filter("objid", sap.ui.model.FilterOperator.NE, result.results[i].objid);
						filters.push(filter);
					}

					andFilter.push(new sap.ui.model.Filter(filters, true));

					Z_OD_SCP_BASRRC001_ORD_SRV.read("/OrdenPuestosTrabajo", {
						filters: andFilter,
						success: function (result) {
							var jsonModel = new sap.ui.model.json.JSONModel(result.results);
							thes.getView().setModel(jsonModel, "modeloPuestosTrabajos");
						},
						error: function (err) {

						}
					});

				},
				error: function (err) {

				}
			});

		},

		newPuestoTrabajo: function () {
			return {
				equip: "",
				objty: "",
				objid: "",
				principal: false
			};
		},

		onEliminarPuestoTrabajo: function (oEvent) {
			var index = thes.byId("smartTablePuestoTrabajo").getTable().getSelectedIndex();

			if (index < 0) {
				return;
			}

			var path = thes.byId("smartTablePuestoTrabajo").getTable().getContextByIndex(index).getPath();
			//var reg = thes.byId("smartTablePuestoTrabajo").getTable().getContextByIndex(index).getProperty(path);

			path = path.replace("ZCDS_VW_BASRRC001_062", "PuestoTrabajoSet");

			Z_OD_SCP_BASRRC001_TAB_SRV.remove(path, {
				success: function (result) {
					sap.m.MessageBox.success("Fue eliminado con éxito!");
					thes.getView().getModel().refresh();
				},
				error: function (err) {
					sap.m.MessageBox.error("Error al eliminar");
					thes.getView().getModel().refresh();
				}
			});

		},

		mostrarMensajes: function (mensajes) {

			/*var mensajes = [{
				"Type": "E",
				"Message": "Mensaje de prueba"
			}];*/

			for (var i = 0; i < mensajes.length; i++) {
				switch (mensajes[i].Type) {
				case "E":
					mensajes[i].Type = "Error";
					break;
				case "S":
					mensajes[i].Type = "Success";
					break;
				case "W":
					mensajes[i].Type = "Warning";
					break;
				case "I":
					mensajes[i].Type = "Information";
					break;
				case "error":
					mensajes[i].Type = "Error";
					break;
				case "success":
					mensajes[i].Type = "Success";
					break;
				case "warning":
					mensajes[i].Type = "Warning";
					break;
				case "information":
					mensajes[i].Type = "Information";
					break;
				}
			}

			var oMessageTemplate = new sap.m.MessageItem({
				type: '{Type}',
				title: '{Message}'
			});

			var oModel = new sap.ui.model.json.JSONModel(mensajes);

			var oBackButton = new sap.m.Button({
				icon: sap.ui.core.IconPool.getIconURI("nav-back"),
				visible: false,
				press: function () {
					thes.oMessageView.navigateBack();
					this.setVisible(false);
				}
			});

			thes.oMessageView = new sap.m.MessageView({
				showDetailsPageHeader: false,
				itemSelect: function () {
					oBackButton.setVisible(true);
				},
				items: {
					path: "/",
					template: oMessageTemplate
				}
			});

			thes.oMessageView.setModel(oModel);

			thes.oDialog = new sap.m.Dialog({
				resizable: true,
				content: thes.oMessageView,
				beginButton: new sap.m.Button({
					press: function () {
						thes.oDialog.close();
					},
					text: "Close"
				}),
				customHeader: new sap.m.Bar({
					contentMiddle: [
						new sap.m.Text({
							text: "Mensajes"
						})
					],
					contentLeft: [oBackButton]
				}),
				contentHeight: "50%",
				contentWidth: "50%",
				verticalScrolling: false
			});

			thes.oMessageView.navigateBack();
			thes.oDialog.open();

		}

	});
});